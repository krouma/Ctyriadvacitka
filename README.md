# Čtyřiadvacítka

Own CMS based on Nette framework

## About application

There are 3 types of web pages

1. Page - has its own presenter, isn't just static (e.g. sends form)
2. Article - static, loaded from DB (e.g. date of events, FAQ)
3. News - special kind of article, has its own DB table (e.g. info about upcoming event)

## Dependencies

1. composer and NPM
2. docker, docker-compose
3. recommended: Git

## Setup

### 1. Add file with DB access data

#### 1.1. Copy file config.local.neon to app/cofig

    cp config/config.local.neon app/config/

#### 1.2. Edit app/config/config.local.neon to real data

Don't add this file to git, it can contain your password!
Access data to docker DB are:

- host: db
- dbname: lamp
- username: lamp
- password: Lamp_001

### 2. Install dependencies

    composer install
    npm install

### 3. Build CSS&JS

    npm run build

## Run app

### 1. Run docker containers

    docker-compose up -d

### 2. Run Node.JS

    npm start

### 3. Web is available at localhost

- localhost:8000 -> backend
- localhost:4000 -> backend with hotreloading assets

## Troubleshooting

### Server exceptions

```bash
docker-compose exec app bash
cat log/exception.log
```

### Refresh after change in latte templates

```bash
docker-compose exec app bash -c 'rm -rf temp/cache/*'
```

## Deploy

We have used `dploy` but it does not work for me anymore (uploads nothing) and
one had to remove Nette cache files from the server.

Now I have been more successful with `ftp-deploy`. It uploads everything again,
but it works.

```
npm run deploy
```

## License

MIT
