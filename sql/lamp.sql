-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE IF NOT EXISTS `lamp` /*!40100 DEFAULT CHARACTER SET utf8
    COLLATE utf8_czech_ci */;
USE `lamp`;


CREATE TABLE `article`
(
    `article_id`  int(11) NOT NULL AUTO_INCREMENT,
    `title`       varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
    `content`     text COLLATE utf8_czech_ci         DEFAULT NULL,
    `url`         varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
    `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
    `requestable` tinyint(4)                         DEFAULT NULL,
    `author`      varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
    `datetime`    datetime                           DEFAULT NULL,
    `in_menu`     tinyint(4)                         DEFAULT NULL,
    PRIMARY KEY (`article_id`),
    UNIQUE KEY `url` (`url`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_czech_ci;


CREATE TABLE `news`
(
    `news_id`     int(11) NOT NULL AUTO_INCREMENT,
    `title`       varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
    `content`     text COLLATE utf8_czech_ci         DEFAULT NULL,
    `url`         varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
    `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
    `author`      varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
    `datetime`    datetime                           DEFAULT NULL,
    PRIMARY KEY (`news_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_czech_ci;


CREATE TABLE `parameters`
(
    `key`   varchar(255) COLLATE utf8_czech_ci NOT NULL,
    `value` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_czech_ci;


CREATE TABLE `user`
(
    `user_id`  int(11)                            NOT NULL AUTO_INCREMENT,
    `username` varchar(255) COLLATE utf8_czech_ci NOT NULL,
    `password` varchar(255) COLLATE utf8_czech_ci NOT NULL,
    `role`     varchar(255) COLLATE utf8_czech_ci NOT NULL DEFAULT 'registered',
    `name`     varchar(255) COLLATE utf8_czech_ci          DEFAULT NULL,
    `surname`  varchar(255) COLLATE utf8_czech_ci          DEFAULT NULL,
    `nickname` varchar(255) COLLATE utf8_czech_ci          DEFAULT NULL,
    `email`    varchar(255) COLLATE utf8_czech_ci          DEFAULT NULL,
    PRIMARY KEY (`user_id`),
    UNIQUE KEY `username` (`username`),
    UNIQUE KEY `user_email_uindex` (`email`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_czech_ci;


CREATE USER 'lamp'@'172.%'
    IDENTIFIED BY PASSWORD '*616447D899FB67566D77F319B512158AD1784460';
GRANT GRANT OPTION, CREATE ROUTINE, CREATE TEMPORARY TABLES, LOCK TABLES, ALTER, CREATE, CREATE VIEW, DELETE, DROP, INDEX, INSERT, REFERENCES, SELECT, SHOW VIEW, TRIGGER, UPDATE, ALTER ROUTINE, EXECUTE ON `lamp`.* TO 'lamp'@'172.%';

-- 2017-03-14 21:05:06
