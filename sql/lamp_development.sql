-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `article` (`article_id`, `title`, `content`, `url`, `description`, `requestable`, `author`, `datetime`, `in_menu`)
VALUES (1, 'Úvod',
        '<p>V&iacute;tejte na na&scaron;em webu!</p>\n\n<p>Tento web je postaven na <strong>jednoduch&eacute;m redakčn&iacute;m syst&eacute;mu v Nette frameworku</strong>. Toto je &uacute;vodn&iacute; čl&aacute;nek načten&yacute; z datab&aacute;ze.</p>\n',
        'uvod', 'Úvodní článek na webu v Nette v PHP', 1, 'mates', '2019-08-15 22:24:58', 1),
       (2, 'Stránka nebyla nalezena',
        '<p>Litujeme, ale požadovan&aacute; str&aacute;nka nebyla nalezena. Zkontrolujte pros&iacute;m URL adresu.</p>\n',
        'chyba', 'Stránka nebyla nalezena.', 0, 'mates','2019-08-15 22:24:58',  0);


INSERT INTO `news` (`news_id`, `title`, `content`, `description`, `author`, `datetime`)
VALUES (1, 'Novinka nenalezena', '<p>Omlouv&aacute;me se, ale novinka nebyla nalezena.</p>\n', 'Novinka nenalezena', 'mates', '2019-08-14 22:25:46'),
       (4, 'Novinky fungují',
        '<p>Po dlouh&eacute;m tr&aacute;pen&iacute; a nalezen&iacute; sabotuj&iacute;c&iacute;ho středn&iacute;ku již v&scaron;e funguje, jak m&aacute;.</p>\n',
        'Přidávání novinek již funguje', 'mates', '2019-08-16 22:11:36'),
       (5, 'Zkouška redaktora', '<p>Zkou&scaron;ka</p>\n', 'Zkouška redaktora', 'mates', '2019-08-15 22:24:58'),
       (6, 'Skrytá novinka', '<p>Novinka</p>\n', 'Důmyslně ukrytá novinka', 'mates', '2019-08-15 22:24:58');

INSERT INTO `parameters` (`key`, `value`)
VALUES ('webName', 'Čtyřiadvacítka'),
       ('webDescription', 'Webové stránky 24. skautského oddílu Hradec Králové');

INSERT INTO `user` (`user_id`, `username`, `password`, `role`, `name`, `surname`, `nickname`, `email`)
VALUES (1, 'mates', '$2y$10$I.30JI7FBJwkeTOKS6AdgO04hdB3l48aNhosEePw8oYW8SBK0sJ9K', 'admin', '', '', '',
        'kroupa.matyas@gmail.com'),
       (2, 'redaktor', '$2y$10$mUUifMjyOw5xxOaIbp/LNeiKMDPXkGQ1VwEnPPqCHT5P.P1uChl1y', 'editor', NULL, NULL, NULL, NULL),
       (8, 'uzivatel', '$2y$10$G0C0vXrLmvaqg5Fqd.2j4.OmSGce1A4dxnDDV4ydwgGfbzKrHg27i', 'registered', NULL, NULL, NULL,
        'kroupa.maty@spoluzaci.cz');

-- 2017-03-08 14:38:07
