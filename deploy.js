const FtpDeploy = require("ftp-deploy");
const ftpDeploy = new FtpDeploy();

const config = {
	user: "ctyriadvacitka",
	host: "ctyriadvacitka.skauting.cz",
	port: 21,
	localRoot: __dirname,
	remoteRoot: "/www/",
	include: ["*", "**/*", ".*"],
	exclude: [
		".htaccess",
		"app/config/config.local.neon",
		"temp/",
		"www/photos/**",
		"www/prilohy/**",
		"node_modules/**",
		"node_modules/**/.*",
		".git/**",
	],
	// delete ALL existing files at destination before uploading, if true
	deleteRemote: false,
	// Passive mode is forced (EPSV command is not sent)
	forcePasv: true,
	// use sftp or ftp
	sftp: false,
};

ftpDeploy
	.deploy(config)
	.then((res) => console.log("finished:", res))
	.catch((err) => console.log(err));
