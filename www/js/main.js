//Init Ajax
$(function() {
  $.nette.init()
  handleMenuToggle()
})

//Hide flash messages after 5 seconds
$(document).ready(function() {
  hideFlashMessage()
})

$.nette.ext({
  success: function() {
    hideFlashMessage()
  },
})

function hideFlashMessage() {
  var n = $('.notification:visible').length
  if (n > 0) {
    setTimeout(function() {
      $('.notification').fadeOut('fast')
    }, 5000) // <-- time in milliseconds
  }
}

function handleMenuToggle() {
  const burgers = document.querySelectorAll('.navbar-burger')
  burgers.forEach(burger => {
    burger.addEventListener('click', () => {
      toggleMenu(burger)
    })
  })
  document.querySelectorAll('.navbar-menu').forEach(menu => {
    menu.addEventListener('click', () => {
      burgers.forEach(burger => {
        toggleMenu(burger)
      })
    })
  })

  function toggleMenu(burger) {
    burger.classList.toggle('is-active')
    const menu = document.getElementById(burger.dataset.target)
    menu.classList.toggle('is-active')
  }
}
