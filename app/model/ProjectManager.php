<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 21.2.17
 * Time: 21:29
 */

namespace App\Model;


use App\CoreModule\Model\ArticleManager;
use Nette\Database\Context;

/**
 * Class ProjectManager
 * @package App\Model
 */
class ProjectManager extends BaseManager
{
    private $table;
    private $wwwDir;

    /**
     * @param string $wwwDir
     * @param Context $database automatically injected class to work with DB
     */
    public function __construct(string $wwwDir, Context $database)
    {
        parent::__construct($database);
        $this->table = $this->database->table('parameters');
        $this->wwwDir = $wwwDir;
    }

    /**
     * @param $key
     * @param $value
     * @internal param array $parameter
     */
    public function saveParameter($key, $value): void
    {
        $parameter = array(
            'key' => $key,
            'value' => $value
        );
        $table = clone $this->table;
        if ($this->isParameterInDB($key)) {
            $table->where('key', $key)->update($parameter);
        } else {
            $table->insert($parameter);
        }
    }

    /**
     * @param $key
     * @return string|mixed|null
     */
    public function getParameter($key)
    {
        $table = clone $this->table;
        $row = $table->where('key', $key)->fetch();
        if ($row === false) {
            return false;
        }
        return $row->value;
    }

    /**
     * @param $key
     * @return bool
     */
    public function isParameterInDB($key): bool
    {
        $table = clone $this->table;
        $value = $table->where('key', $key)->fetch();
        if ($value) {
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getWwwDir(): string
    {
        return $this->wwwDir;
    }

    /**
     * @return array
     */
    public function getArticlesInMenu(): array
    {
        $articles = $this->database->table(ArticleManager::TABLE_NAME)->fetchAll();
        $articlesInMenu = [];

        foreach ($articles as $article) {
            if ($article->in_menu) {
                $articlesInMenu[$article->url] = $article->title;
            }
        }
        return $articlesInMenu;
    }
}