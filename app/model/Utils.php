<?php

declare(strict_types=1);

namespace App\Model;


use Nette\Utils\DateTime;

class Utils
{
    /**
     * Returns date in d.m.Y from datetime
     * @param DateTime $datetime
     * @return string
     */
    public static function formatDate(DateTime $datetime): string
    {
        return date('d.m.Y', DateTime::from($datetime)->getTimestamp());
    }
}