<?php

declare(strict_types=1);

namespace App\Factories;


use Nette\Application\UI\Form;
use Nette\Http\FileUpload;
use Nette\Utils\FileSystem;

class ArticleImagesFormFactory extends ImageUploadFormFactory
{
    /**
     * @param Form $form
     */
    public function processForm(Form $form): void
    {
        $images = $form->getValues()->newPictures;
        $dir = __DIR__ . '/../../www/prilohy/' . date('Y') . '/';
        FileSystem::createDir($dir, 0755);

        /** @var FileUpload $image */
        foreach ($images as $image) {
            $image->move($dir . $image->name);
        }
    }
}