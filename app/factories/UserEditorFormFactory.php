<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 16.1.18
 * Time: 21:51
 */

namespace App\Factories;


use Nette\Application\UI\Form;

class UserEditorFormFactory extends BaseFormFactory
{
    public function create(): Form
    {
        $form = parent::create();
        $form->addHidden('user_id');
        $form->addText('name', 'Jméno');
        $form->addText('surname', 'Příjmení');
        $form->addText('nickname', 'Přezdívka');
        $form->addText('username', 'Přihlašovací jméno')->setRequired();
        $form->addEmail('email', 'E-mail')->setRequired();
        $form->addHidden('role');
        $form->addSubmit('submit', 'Uložit');
        return $form;
    }

}