<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 15.1.18
 * Time: 21:19
 */

namespace App\Factories;



use Nette\Application\UI\Form;

class SignInFormFactory extends BaseFormFactory
{
    /**
     * @return Form
     */
    public function create(): Form
    {
        $form = parent::create();
        $form->addText('username', 'Přihlašovací jméno')->setRequired();
        $form->addPassword('password', 'Heslo')->setRequired();
        $form->addSubmit('submit', 'Přihlásit');
        return $form;
    }
}