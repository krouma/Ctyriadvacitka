<?php

declare(strict_types=1);

namespace App\Factories;


use App\CoreModule\Model\ArticleManager;
use Nette\Application\UI\Form;

class ArticleEditorFormFactory extends BaseFormFactory
{
    public function create(): Form
    {
        $form = parent::create();
        $form->addHidden(ArticleManager::COLUMN_ID);
        $form->addText('title', 'Titulek')->setRequired();
        $form->addText('url', 'URL')->setRequired();
        $form->addText('datetime', 'Datum')->setRequired()->setDefaultValue(date('j.n.Y'));
        $form->addCheckbox('requestable', 'Zobrazovat v seznamu')
            ->setDefaultValue(true);
        $form->addTextArea('content', 'Obsah')->setRequired();
        $form->addSubmit('submit', 'Uložit článek');
        return $form;
    }
}