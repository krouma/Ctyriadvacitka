<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 25.4.17
 * Time: 15:19
 */

namespace App\Factories;


use App\Model\ProjectManager;
use Nette\Application\UI\Form;

class WebPropertiesFormFactory extends BaseFormFactory
{
    private $projectManager;

    public function __construct(ProjectManager $projectManager)
    {
        $this->projectManager = $projectManager;
    }

    public function create(): Form
    {
        $form = parent::create();
        $form->getElementPrototype()->setAttribute('class', 'ajax');
        $form->addText('webName', 'Jméno webu')
            ->setRequired();
        $form->addText('webDescription', 'Popis webu')
            ->setRequired();
        $form->addSubmit('submit', 'Uložit')
            ->setAttribute('class', 'ajax is-primary');
        $form->onSuccess[] = function (Form $form, array $values){
            foreach ($values as $key => $value) {
                $this->projectManager->saveParameter($key, $value);
            }
        };
        return $form;
    }
}