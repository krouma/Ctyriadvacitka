<?php

declare(strict_types=1);

namespace App\Factories;


use Nette\Application\UI\Form;

class NewsEditorFormFactory extends BaseFormFactory
{

    public function create(): Form
    {
        $form = parent::create();
        $form->addHidden('news_id');
        $form->addText('title', 'Titulek')->setRequired();
        $form->addText('datetime', 'Datum')->setRequired()->setDefaultValue(date('j.n.Y'));
        $form->addTextArea('content', 'Obsah')->setRequired();
        $form->addSubmit('submit', 'Uložit článek');
        return $form;
    }
}