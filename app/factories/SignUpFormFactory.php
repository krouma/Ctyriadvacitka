<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 15.1.18
 * Time: 21:48
 */

namespace App\Factories;



use Nette\Application\UI\Form;

class SignUpFormFactory extends BaseFormFactory
{
    public function create(): Form
    {
        $form = parent::create();
        $form->addText('username', 'Přihlašovací jméno')->setRequired();
        $form->addPassword('password', 'Heslo')->setRequired();
        $form->addPassword('passwordAgain', 'Heslo znovu')->setRequired()
            ->addRule(Form::EQUAL, 'Hesla se neschodují!', $form['password']);
        $form->addEmail('email', 'E-mail')->setRequired();
        $form->addSubmit('submit', 'Registrovat');
        return $form;
    }
}