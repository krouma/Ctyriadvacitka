<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 14.3.17
 * Time: 18:31
 */

namespace App\Factories;


use Nette\Application\UI\Form;
use Nette\Http\FileUpload;

/**
 * Class HeaderImagesForm
 * @package App\Model
 */
class HeaderImagesFormFactory extends ImageUploadFormFactory
{
    /**
     * @param Form $form
     */
    public function processForm(Form $form): void
    {
        $images = $form->getValues()->newPictures;

        /** @var FileUpload $image */
        foreach ($images as $image) {
            $image->move(__DIR__ . '/../../www/img/top/' . $image->name);
        }
    }
}