<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 17.1.18
 * Time: 20:37
 */

namespace App\Factories;


use Nette\Application\UI\Form;

class ChangePasswordFormFactory extends BaseFormFactory
{
    public function create(): Form
    {
        $form = parent::create();
        $form->addPassword('currentPassword', 'Aktuální heslo')
            ->setRequired();
        $form->addPassword('newPassword', 'Nové heslo')
            ->setRequired();
        $form->addPassword('newPasswordAgain', 'Nové heslo znovu')
            ->setRequired()
            ->addRule(Form::EQUAL, 'Hesla se neschodují!', $form['newPassword']);
        $form->addSubmit('submit', 'Změnit heslo');
        return $form;
    }
}