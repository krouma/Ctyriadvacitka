<?php

declare(strict_types=1);

namespace App\Factories;


use Nette\Application\UI\Form;

class ImageUploadFormFactory extends BaseFormFactory
{
    /**
     * @return Form
     */
    public function create(): Form
    {
        $form = parent::create();
        $form->getElementPrototype()->setAttribute('class', 'ajax');

        $form->addMultiUpload('newPictures', 'Přidat nové obrázky (lze více najednou)')
            ->setRequired('Musíte vybrat nejméně 1 obrázek!')
            ->addRule(Form::IMAGE, 'Obrázek musí být ve formátu JPEG, PNG nebo GIF.');

        $form->addSubmit('submit', 'Přidat');
        $form->onSuccess[] = [$this, 'processForm'];

        return $form;
    }
}