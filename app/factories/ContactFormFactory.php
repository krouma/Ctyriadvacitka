<?php

declare(strict_types=1);

namespace App\Factories;


use Nette\Application\UI\Form;

class ContactFormFactory extends BaseFormFactory
{
    public function create(): Form
    {
        $form = parent::create();
        $form->addText('username', "Vaše jméno")->setRequired();
        $form->addText('email', 'Email')->setType('email')->setRequired();
        $form->addTextArea('message', 'Zpráva')->setRequired()
            ->setHtmlAttribute('placeholder', "Vaše zpráva")
            ->addRule(Form::MIN_LENGTH, 'Zpráva musí být minimálně %d znaků dlouhá.', 10);
        $form->addSubmit('submit', 'Odeslat');
        return $form;
    }
}