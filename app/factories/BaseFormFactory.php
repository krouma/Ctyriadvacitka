<?php

declare(strict_types=1);

namespace App\Factories;


use App\Renderers\SkautDesignFormRenderer;
use Nette\Application\UI\Form;

abstract class BaseFormFactory
{

    public function create(): Form
    {
        $form = new Form();
        $form->setRenderer(new SkautDesignFormRenderer());
        return $form;
    }

}