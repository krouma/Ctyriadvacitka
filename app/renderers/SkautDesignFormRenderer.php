<?php

declare(strict_types=1);

namespace App\Renderers;


use Nette\Forms\Rendering\DefaultFormRenderer;

class SkautDesignFormRenderer extends DefaultFormRenderer
{
    public function renderBegin(): string
    {
        // Divs
        $this->wrappers['controls']['container'] = null;
        $this->wrappers['pair']['container'] = 'div class="field"';
        $this->wrappers['control']['container'] = 'div class="control"';

        // Classes
        foreach ($this->form->getControls() as $control) {
            $type = $control->getOption('type');
            if ($type === 'button') {
                $control->getControlPrototype()->addClass('button is-primary');
            } elseif ($type === 'text') {
                $control->getControlPrototype()->addClass('input is-medium');
                $control->getLabelPrototype()->addClass('label is-primary');
            } elseif ($type === 'textarea') {
                $control->getControlPrototype()->addClass('textarea is-medium');
                $control->getLabelPrototype()->addClass('label is-primary');
            }
        }

        $this->form->getElementPrototype()->addAttributes([
            'class' => 'column'
        ]);

        return parent::renderBegin();
    }

}