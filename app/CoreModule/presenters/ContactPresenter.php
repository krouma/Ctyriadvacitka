<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: matyas
 * Date: 9.10.16
 * Time: 12:48
 */

namespace App\CoreModule\Presenters;

use App\Factories\ContactFormFactory;
use App\Presenters\BasePresenter;
use Nette\Application\UI\Form;
use Nette\InvalidStateException;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Nette\Utils\ArrayHash;

/**
 * Zpracovává kontaktní formulář
 * Class ContactPresenter
 * @package App\CoreModule\Presenters
 */
class ContactPresenter extends BasePresenter {

    /**Email administrátora, na který se budou posílat emaily z kontaktního formuláře. */
    const EMAIL = 'kroupa.matyas@gmail.com';
    const EMAIL2 = 'lukas.trumm@gmail.com';

    /** @var ContactFormFactory */
    private $contactFormFactory;

    /**
     * ContactPresenter constructor.
     * @param ContactFormFactory $contactFormFactory
     */
    public function __construct(ContactFormFactory $contactFormFactory)
    {
        parent::__construct();
        $this->contactFormFactory = $contactFormFactory;
    }

    /**
     * Vytváří a vrací komponentu kontaktního formuláře
     * @return Form kontaktní formulář
     */
    protected function createComponentContactForm():Form{
        $form = $this->contactFormFactory->create();
        $form->onSuccess[] = [$this, 'contactFormSucceeded'];
        return $form;
    }

    /**
     * Funkce se vykoná při úspěšném odeslání kontaktního formuláře a odeěle e-mail.
     * @param Form $form kontaktní formulář
     * @param ArrayHash $values odeslané hodnoty formuláře
     * @throws \Nette\Application\AbortException
     */
    public function contactFormSucceeded(Form $form, ArrayHash $values): void
    {
        try{
            $mail = new Message();
            $mail->setFrom($values->email)
                ->addTo(self::EMAIL)
                ->addTo(self::EMAIL2)
                ->setSubject('Email z BasicRS')
                ->setBody($values->message);
            $mailer = new SendmailMailer();
            $mailer->send($mail);
            $this->flashMessage('E-mail byl úspěšně odeslán.', 'success');
            $this->redirect('this');
        }
        catch (InvalidStateException $exception){
            $this->flashMessage('E-mail se nepodařilo odeslat.', 'danger');
        }
    }
}
