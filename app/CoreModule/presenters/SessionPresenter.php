<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 19.11.16
 * Time: 22:04
 */

namespace App\CoreModule\Presenters;


use App\Factories\SignInFormFactory;
use App\Factories\SignUpFormFactory;
use App\Presenters\BasePresenter;
use Nette\Database\UniqueConstraintViolationException;
use Nette\Forms\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\Passwords;

/**
 * @package App\CoreModule\Presenters
 * @author matyas
 */
class SessionPresenter extends BasePresenter
{
    /** @var SignInFormFactory @inject */
    public $signInFormFactory;
    /** @var SignUpFormFactory @inject */
    public $signUpFormFactory;
    /** @var Passwords @inject */
    public $passwords;

    /**
     * @throws \Nette\Application\AbortException
     */
    public function renderSignIn(): void
    {
        $this->redirectIfLoggedUser();
    }

    /**
     * @return Form
     */
    public function createComponentSignInForm(): Form
    {
        $form = $this->signInFormFactory->create();
        $form->onSuccess[] = function (Form $form) {
            try {
                $values = $form->getValues();
                $this->user->login($values['username'], $values['password']);
                $this->flashMessage('Přihlášení proběhlo úspěšně.', 'success');
                $this->redirect(':Admin:Web:default');
            } catch (AuthenticationException $e) {
                $this->flashMessage($e->getMessage(), 'warning');
            }
        };
        return $form;
    }

    /**
     * @throws \Nette\Application\AbortException
     */
    public function actionSignOut(): void
    {
        $this->setLayout(false);
        if (!$this->user->isLoggedIn()) {
            $this->flashMessage('Není přihlášen žádný uživatel.', 'info');
            $this->redirect(':Core:Article:');
        } else {
            $this->user->logout(true);
            $this->flashMessage('Uživatel úspěšně odhlášen.', 'success');
            $this->redirect(':Core:Article:');
        }
    }

    /**
     * @throws \Nette\Application\AbortException
     */
    public function actionSignUp(): void
    {
        $this->redirectIfLoggedUser();
    }

    /**
     * @return Form formulář k registraci
     */
    public function createComponentSignUpForm(): Form
    {
        $form = $this->signUpFormFactory->create();
        $form->onSuccess[] = function (Form $form) {
            $values = $form->getValues();
            $username = $values['username'];
            $password = $this->passwords->hash($values['password']);
            $user = array(
                'username' => $username,
                'password' => $password,
                'role' => 'registered',
                'email' => $values['email']);
            try {
                $this->userManager->saveUser($user);
                $this->flashMessage('Registrace proběhla úspěšně, nyní se přihlašte.', 'success');
                $this->redirect(':Core:Session:signIn');
            } catch (UniqueConstraintViolationException $e) {
                $this->flashMessage('Uživatel s tímto přihlašovacím jménem nebo e-mailem již existuje.', 'warning');
            }
        };
        return $form;
    }

    /**
     * It will redirect to user profile if user is logged in
     * @throws \Nette\Application\AbortException
     */
    public function redirectIfLoggedUser(): void
    {
        if ($this->user->isLoggedIn()) {
            $username = $this->user->getIdentity()->username;
            $this->flashMessage("Už je přihlášen uživatel $username.", 'info');
            $this->redirect(':Core:User:', $username);
        }
    }
}
