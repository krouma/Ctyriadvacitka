<?php
declare(strict_types=1);

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\CoreModule\Presenters;

use App\Presenters\BasePresenter;
use Nette\Utils\Paginator;

/**
 * @package App\CoreModule\Presenters
 * @author matyas
 */
class ArticlePresenter extends BasePresenter
{
    /**
     * Displays article by url
     * @param string $url URL článku
     */
    public function renderDefault(string $url = NULL): void
    {
        //Will try to load article with $url, otherwise throws 404
        $article = $this->articleManager->getArticleByUrl($url);
        if ($article === null) {
            $article = $this->articleManager->getArticleByUrl('chyba');
            $this->template->originalUrl = $url;
        }
        if (!($this->user->isInRole('admin') || $this->user->isInRole('editor') || $article->requestable)) {
            $article = $this->articleManager->getArticleByUrl('chyba');
        }
        $this->template->article = $article;
    }

    /**
     * Renders list of news on pages. There are 4 news in ine page
     * @param int $page page of list
     */
    public function renderPagedList(int $page = 1): void
    {
        $articlesCount = $this->articleManager->getArticlesCount();

        $paginator = new Paginator();
        $paginator->setItemCount($articlesCount);
        $paginator->setItemsPerPage(10);
        $paginator->setPage($page);

        $articles = $this->articleManager->getLimitedArticles($paginator->getLength(), $paginator->getOffset());

        $this->template->articles = $articles;
        $this->template->activePage = $page;
        $this->template->pages = $paginator->getPageCount();
    }
}
