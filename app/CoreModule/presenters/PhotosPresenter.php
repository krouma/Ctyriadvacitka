<?php
declare(strict_types=1);

namespace App\CoreModule\Presenters;

use App\Factories\ContactFormFactory;
use App\Presenters\BasePresenter;
use Nette\Application\UI\Form;
use Nette\InvalidStateException;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Nette\Utils\ArrayHash;

/**
 * Stranka s fotkama
 */
class PhotosPresenter extends BasePresenter {

    public function __construct()
    {
        parent::__construct();
    }

    public function renderDefault() {}

}
