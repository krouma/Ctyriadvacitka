<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: matyas
 * Date: 4.11.16
 * Time: 18:39
 */

namespace App\CoreModule\Presenters;


use App\Factories\ChangePasswordFormFactory;
use App\Factories\UserEditorFormFactory;
use App\Presenters\BasePresenter;
use Nette\Application\UI\Form;
use Nette\Database\UniqueConstraintViolationException;
use Nette\Security\AuthenticationException;

/**
 * @package App\CoreModule\Presenters
 * $author matyas
 */
class UserPresenter extends BasePresenter
{
    /** @var UserEditorFormFactory $userEditorFormFactory @inject */
    public $userEditorFormFactory;
    /** @var ChangePasswordFormFactory @inject */
    public $changePasswordFormFactory;

    /**
     * Loads user by username
     * @param string|null $username
     * @throws \Nette\Application\AbortException
     */
    public function renderDefault(string $username = null): void
    {
        if (!$username) {
            if ($this->user->isLoggedIn()) {
                $this->redirect(':Core:User:', $this->user->getIdentity()->username);
            } else {
                $this->flashMessage('Nespecifikován uživatel');
                $this->redirect(':Core:News:pagedList');
            }
        } else {
            $user = $this->userManager->getUserByUsername($username);
            if ($user === null){
                $this->flashMessage('Uživatel nebyl nalezen.', 'warning');
                $this->redirect(':Core:News:pagedList');
            }
            $this->template->user = $user;
        }
    }

    /**
     * Edits user
     * @param string $username jméno uživatele, kterého editujeme
     * @throws \Nette\Application\AbortException
     */
    public function renderEditor(string $username = null): void
    {
        $this->logInRequired();
        if ($username) ($user = $this->userManager->getUserByUsername($username)) ? $this['userEditorForm']->setDefaults($user) : $this->flashMessage('Uživatel nebyl nalezen');

        //If user is logged but does not fill username in URL, redirect to edit themselves
        if (!$username && $this->user->isLoggedIn()) {
            $user = $this->userManager->getUserById($this->user->getId());
            $this['userEditorForm']->setDefaults($user);
        }

        if ($this->user->id !== $user['user_id']) {
            $this->flashMessage('Nemůžete upravovat jiné uživatele!', 'danger');
            $this->redirect(':Core:User:', $this->user->getIdentity()->username);
        }
        $this->template->user = $user;
    }

    /**
     * @return Form
     */
    protected function createComponentUserEditorForm(): Form
    {
        $form = $this->userEditorFormFactory->create();
        $form->onSuccess[] = function (Form $form) {
            $values = $form->getValues(true);
            try {
                $this->userManager->saveUser($values);
                $this->flashMessage('Uživatel byl úspěšně editován.', 'success');
                $this->redirect(':Core:User:', $values['username']);
            } catch (UniqueConstraintViolationException $exception) {
                $this->flashMessage('Uživatel s tímto jménem nebo emailem již existuje.', 'warning');
            }
        };
        return $form;
    }

    /**
     * Changes password of user
     * @param $username
     * @throws \Nette\Application\AbortException
     */
    public function renderChangePassword($username): void
    {
        $this->logInRequired();
        if ($this->user->getIdentity()->username !== $username) {
            $this->flashMessage('Změna hesla je možná jen u sebe.');
            $this->redirect(':Core:User:', $username);
        }
    }

    /**
     * Creates form for changing password of logged user
     * @return Form
     */
    public function createComponentChangePasswordForm(): Form
    {
        $form = $this->changePasswordFormFactory->create();
        $form->onSuccess[] = function (Form $form) {
            $values = $form->getValues(true);
            try {
                $username = $this->user->getIdentity()->username;
                $this->userManager->changePassword($username, $values['currentPassword'], $values['newPassword']);
                $this->flashMessage('Heslo bylo úspěšně změněno.', 'success');
                $this->redirect(':Core:User:');
            } catch (AuthenticationException $e) {
                $this->flashMessage($e->getMessage(), 'danger');
            }
        };
        return $form;
    }
}
