<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 21.1.17
 * Time: 20:49
 */

namespace App\CoreModule\Presenters;


use App\Presenters\BasePresenter;
use Nette\Utils\Paginator;

/**
 * Processes rendering news
 * Class NewsPresenter
 * @package App\CoreModule\Presenters
 */
class NewsPresenter extends BasePresenter
{
    public function renderPagedList(int $page = 1): void
    {
        $newsCount = $this->newsManager->getNewsCount();

        $paginator = new Paginator();
        $paginator->setItemCount($newsCount);
        $paginator->setItemsPerPage(5);
        $paginator->setPage($page);

        $news = $this->newsManager->getLimitedNews($paginator->getLength(), $paginator->getOffset());

        $this->template->articles = $news;
        $this->template->activePage = $page;
        $this->template->pages = $paginator->getPageCount();
    }
}
