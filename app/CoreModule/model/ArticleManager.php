<?php
declare(strict_types=1);

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\CoreModule\Model;

use App\Model\BaseManager;
use Nette\Database\Table\ActiveRow;
use Nette\Utils\DateTime;

/**
 * Třída poskytuje metody pro správu článků v RS
 * @package App\CoreModule\Module
 * @author matyas
 */
class ArticleManager extends BaseManager
{
    public const
        TABLE_NAME = 'article',
        COLUMN_ID = 'article_id',
        COLUMN_TITLE = 'title',
        COLUMN_CONTENT = 'content',
        COLUMN_URL = 'url',
        COLUMN_REQUESTABLE = 'requestable',
        COLUMN_AUTHOR = 'author',
        COLUMN_DATETIME = 'datetime',
        COLUMN_IN_MENU = 'in_menu';

    /**
     * @return array
     */
    public function getArticles(): array
    {
        return $this->database->table(self::TABLE_NAME)->order(self::COLUMN_DATETIME . ' DESC')->fetchAll();
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getLimitedArticles(int $limit, int $offset): array
    {
        return $this->database->table(self::TABLE_NAME)->order(self::COLUMN_DATETIME . ' DESC')->limit($limit, $offset)->fetchAll();
    }

    /**
     * @return int
     */
    public function getArticlesCount(): int
    {
        return $this->database->table(self::TABLE_NAME)->count();
    }

    /**
     * @param string $url
     * @return ActiveRow
     */
    public function getArticleByUrl(string $url): ?ActiveRow
    {
        return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_URL, $url)->fetch();
    }

    /**
     * @param array $values
     */
    public function saveArticle(array $values): void
    {
        $values[self::COLUMN_DATETIME] = DateTime::createFromFormat('d.m.Y', $values[self::COLUMN_DATETIME]);
        if (empty($values[self::COLUMN_ID])) {
            $values[self::COLUMN_ID] = null;
            $this->database->table(self::TABLE_NAME)->insert($values);
        } else {
            $this->database->table(self::TABLE_NAME)->wherePrimary($values[self::COLUMN_ID])->update($values);
        }
    }

    /**
     * @param string $url
     */
    public function removeArticle(string $url): void
    {
        $this->database->table(self::TABLE_NAME)->where(self::COLUMN_URL, $url)->delete();
    }

    /**
     * @param string $url
     * @param bool $visibility
     */
    public function setVisibilityInMenu(string $url, bool $visibility): void
    {
        $this->database->table(self::TABLE_NAME)->where(self::COLUMN_URL, $url)->update([self::COLUMN_IN_MENU => $visibility]);
    }

}
