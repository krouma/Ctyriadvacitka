<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 6.12.16
 * Time: 16:23
 */

namespace App\CoreModule\Model;


use Nette\Security\Permission;

class AuthorizatorManager extends Permission
{
    public function __construct()
    {
        //Roles
        $this->addRole('guest');
        $this->addRole('registered', 'guest');
        $this->addRole('editor', 'registered');

        $this->addRole('scout', 'registered');
        $this->addRole('leader', 'scout');

        $this->addRole('admin', ['editor', 'leader']);

        //Resources
        $this->addResource('article');
        $this->addResource('user');
        $this->addResource('web');

        $this->addResource('parents');

        //Permissions
        $this->allow('guest', ['article', 'user'], 'view');
        $this->allow('editor', 'article', 'add');

        $this->allow('leader', 'parents', ['show', 'edit', 'sendMail']);

        $this->allow('admin', ['article', 'web'], 'edit');
        $this->allow('admin', 'user', 'remove');
    }
}
