<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 21.1.17
 * Time: 17:34
 */

namespace App\CoreModule\Model;


use App\Model\BaseManager;
use Nette\Database\Table\ActiveRow;
use Nette\Utils\DateTime;

/**
 * Service to work with News
 * Class NewsManager
 * @package App\CoreModule\Model
 */
class NewsManager extends BaseManager
{
    public const
        TABLE_NAME = 'news',
        COLUMN_ID = 'news_id',
        COLUMN_TITLE = 'title',
        COLUMN_CONTENT = 'content',
        COLUMN_AUTHOR = 'author',
        COLUMN_DATETIME = 'datetime';

    /**
     * @return array
     */
    public function getNews(): array
    {
        return $this->database->table(self::TABLE_NAME)->order(self::COLUMN_DATETIME . ' DESC')->fetchAll();
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getLimitedNews(int $limit, int $offset): array
    {
        return $this->database->table(self::TABLE_NAME)->order(self::COLUMN_DATETIME . ' DESC')->limit($limit, $offset)->fetchAll();
    }

    /**
     * @return int
     */
    public function getNewsCount(): int
    {
        return $this->database->table(self::TABLE_NAME)->count();
    }

    /**
     * @param int $id
     * @return ActiveRow
     */
    public function getNewsById(int $id): ?ActiveRow
    {
        return $this->database->table(self::TABLE_NAME)->wherePrimary($id)->fetch();
    }

    /**
     * @param array $values
     */
    public function saveNews(array $values): void
    {
        $values[self::COLUMN_DATETIME] = DateTime::createFromFormat('d.m.Y', $values[self::COLUMN_DATETIME]);
        if (empty($values[self::COLUMN_ID]))
        {
            $values[self::COLUMN_ID] = null;
            $this->database->table(self::TABLE_NAME)->insert($values);
        } else {
            $this->database->table(self::TABLE_NAME)->where(self::COLUMN_ID, $values[self::COLUMN_ID])->update($values);
        }
    }

    /**
     * @param int $id
     */
    public function removeNews(int $id): void
    {
        $this->database->table(self::TABLE_NAME)->wherePrimary($id)->delete();
    }


}
