<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: matyas
 * Date: 4.11.16
 * Time: 21:23
 */

namespace App\CoreModule\Model;


use App\Model\BaseManager;
use Nette\Database\Context;
use Nette\Database\Table\ActiveRow;
use Nette\Security\AuthenticationException;
use Nette\Security\Passwords;

/**
 * User management
 * @package App\CoreModule\Model
 * @author matyas
 */
class UserManager extends BaseManager
{
    public const
        TABLE_NAME = 'user',
        COLUMN_ID = 'user_id',
        COLUMN_USERNAME = 'username',
        COLUMN_PASSWORD_HASH = 'password',
        COLUMN_ROLE = 'role',
        COLUMN_NAME = 'name',
        COLUMN_SURNAME = 'surname',
        COLUMN_NICKNAME = 'nickname',
        COLUMN_EMAIL = 'email';

    private $passwords;

    public function __construct(Context $database, Passwords $passwords)
    {
        parent::__construct($database);
        $this->passwords = $passwords;
    }

    /**
     * @return array
     */
    public function getUsers(): array
    {
        return $this->database->table(self::TABLE_NAME)->fetchAll();
    }

    /**
     * @param string $username
     * @return ActiveRow
     */
    public function getUserByUsername(string $username): ?ActiveRow
    {
        return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_USERNAME, $username)->fetch();
    }

    /**
     * @param int $id
     * @return ActiveRow
     */
    public function getUserById(int $id): ?ActiveRow
    {
        return $this->database->table(self::TABLE_NAME)->wherePrimary($id)->fetch();
    }

    /**
     * @param array $values
     */
    public function saveUser(array $values): void
    {
        if (isset($values[self::COLUMN_ID])) {
            $this->database->table(self::TABLE_NAME)->wherePrimary($values[self::COLUMN_ID])->update($values);
        } else {
            $this->database->table(self::TABLE_NAME)->insert($values);
        }
    }

    /**
     * @param string $username
     */
    public function removeUser(string $username): void
    {
        $this->database->table(self::TABLE_NAME)->where(self::COLUMN_USERNAME, $username)->delete();
    }

    /**
     * Changes password of user
     * @param string $username of user to change password
     * @param string $currentPassword
     * @param string $newPassword
     * @throws AuthenticationException if current password is not correct
     */
    public function changePassword(string $username, string $currentPassword, string $newPassword): void
    {
        $row = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_USERNAME, $username)->fetch();
        if ($this->passwords->verify($currentPassword, $row->password)) {
            $this->database->table(self::TABLE_NAME)->where(self::COLUMN_USERNAME, $username)->update(array(
                self::COLUMN_PASSWORD_HASH => $this->passwords->hash($newPassword)
            ));
        } else {
            throw new AuthenticationException('Současné heslo neodpovídá zadanému současnému heslu!');
        }
    }

    public function getRoleDisplayName(string $role): string
    {
        $rolesEN = ['registered', 'editor', 'admin'];
        $rolesCS = ['uživatel', 'redaktor', 'administrátor'];
        return $rolesCS[array_search($role, $rolesEN, true)];
    }

}
