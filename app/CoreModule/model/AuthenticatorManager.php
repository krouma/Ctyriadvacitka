<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 5.12.16
 * Time: 21:49
 */

namespace App\CoreModule\Model;


use App\Model\BaseManager;
use Nette\Database\Context;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;
use Nette\Security\Passwords;

/**
 * Class AuthenticatorManager
 * @package App\CoreModule\Model
 */
class AuthenticatorManager extends BaseManager implements IAuthenticator
{

    /**
     * Performs an authentication against e.g. database.
     * and returns IIdentity on success or throws AuthenticationException
     * @return IIdentity
     * @throws AuthenticationException
     */
    public $database;
    private $passwords;

    /**
     * AuthenticatorManager constructor.
     * @param Context $database gets database connection from DI
     * @param Passwords $passwords
     */
    public function __construct(Context $database, Passwords $passwords)
    {
        parent::__construct($database);
        $this->database = $database;
        $this->passwords = $passwords;
    }

    /**
     * Přihlásí uživatele pokud existuje a zadal správné heslo
     * @param array $credentials username and password of user
     * @return Identity instance of user
     * @throws AuthenticationException
     */
    public function authenticate(array $credentials): IIdentity
    {
        list($username, $password) = $credentials;
        $row = $this->database->table('user')->where('username', $username)->fetch();

        if (!$row) {
            throw new AuthenticationException('Uživatel nenalezen.');
        }

        if (!$this->passwords->verify($password, $row->password)) {
            throw new AuthenticationException('Špatné heslo!');
        }

        return new Identity($row->user_id, $row->role, ['username' => $row->username]);
    }
}
