<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: matyas
 * Date: 9.10.16
 * Time: 11:59
 */

namespace App\AdminModule\Presenters;


use App\Factories\HeaderImagesFormFactory;
use App\Factories\WebPropertiesFormFactory;
use App\Presenters\BasePresenter;
use Nette\Forms\Form;

/**
 * Zpracovává vykreslování administrační sekce
 * Class AdministrationPresenter
 * @package App\CoreModule\Presenters
 */
class WebPresenter extends BasePresenter
{
    /** @var  HeaderImagesFormFactory @inject */
    public $headerImagesFormFactory;
    /** @var  WebPropertiesFormFactory @inject */
    public $webPropertiesFormFactory;

    public function startup(): void
    {
        parent::startup();
        $this->logInRequired();
    }

    public function renderDefault(): void
    {
        $this->adminPermissionsRequired();
        $properties['webName'] = $this->projectManager->getParameter('webName');
        $properties['webDescription'] = $this->projectManager->getParameter('webDescription');
        $this['webPropertiesForm']->setDefaults($properties);
    }

    public function renderView(): void
    {
        $this->editorPermissionsRequired();
    }

    /**
     * @return Form
     */
    public function createComponentWebPropertiesForm(): Form
    {
        $form = $this->webPropertiesFormFactory->create();
        $form->onSuccess[] = function (Form $form) {
            $this->flashMessage('Údaje byly upraveny', 'success');
            $this->redirectAjax();
        };
        $form->onError[] = function () {
            $this->redirectAjax(['flashes', 'webPropertiesForm']);
        };
        return $form;
    }
}
