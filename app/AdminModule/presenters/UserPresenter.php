<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 3.3.17
 * Time: 20:42
 */

namespace App\AdminModule\Presenters;


use App\CoreModule\Model\UserManager;
use App\Presenters\BasePresenter;

/**
 * Class UserPresenter
 * @package App\AdminModule\Presenters
 */
class UserPresenter extends BasePresenter
{
    public function startup(): void
    {
        parent::startup();
        $this->logInRequired();
        $this->adminPermissionsRequired();
    }

    public function renderManagement(): void
    {
        $users = $this->userManager->getUsers();
        $this->template->users = $users;
    }

    /**
     * @param string $role
     * @param string $username
     * @throws \Nette\Application\AbortException
     */
    public function handleChangeRole(string $role, string $username): void
    {
        $this->userManager->saveUser(array(
            UserManager::COLUMN_ID => $this->userManager->getUserByUsername($username)->getPrimary(),
            UserManager::COLUMN_ROLE => $role
        ));
        $roleCS = $this->userManager->getRoleDisplayName($role);

        $this->flashMessage("Uživatel $username je nyní $roleCS", 'success');
        $this->redirectAjax(['userManagementTable', 'flashes']);
    }

    /**
     * @param string $username
     * @throws \Nette\Application\AbortException
     */
    public function handleRemove(string $username): void
    {
        $this->logInRequired();
        if (!$this->user->isAllowed('user', 'remove')) {
            $this->flashMessage('Nemůžete mazat uživatele!', 'danger');
            $this->redirectAjax(['flashes']);
        }
        $this->userManager->removeUser($username);
        $this->flashMessage('Uživatel byl úspěšně odstraněn.', 'success');
        $this->redirectAjax(['userManagementTable', 'flashes']);
    }
}