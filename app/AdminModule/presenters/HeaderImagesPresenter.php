<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 15.3.17
 * Time: 17:27
 */

namespace App\AdminModule\Presenters;


use App\Factories\HeaderImagesFormFactory;
use App\Presenters\BasePresenter;
use Nette\Forms\Form;

/**
 * Class HeaderImages
 * @package App\AdminModule\Presenters
 */
class HeaderImagesPresenter extends BasePresenter
{
    /** @var  HeaderImagesFormFactory @inject */
    public $headerImagesFormFactory;

    public function startup(): void
    {
        parent::startup();
        $this->logInRequired();
        $this->adminPermissionsRequired();
    }

    public function renderDefault(): void
    {
        $this->template->headerImages = $this->imageManager->getHeaderImageNames();
    }

    /**
     * @action default
     * @param $name
     * @throws \Nette\Application\AbortException
     */
    public function handleRemove($name): void
    {
        $this->imageManager->deleteImage($this->projectManager->getWwwDir() . "/img/top/$name");
        $this->flashMessage('Obrázek odstraněn.', 'success');
        $this->redirectAjax(['flashes', 'headerImages']);
    }

    /**
     * @return Form
     */
    public function createComponentAddHeaderImagesForm(): Form
    {
        $form = $this->headerImagesFormFactory->create();
        $form->onSuccess[] = function (Form $form) {
            $this->flashMessage('Obrázek přidán.', 'success');
            $this->redirectAjax(['flashes', 'headerImages']);
        };
        return $form;
    }
}