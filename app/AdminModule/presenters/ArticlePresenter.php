<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 5.3.17
 * Time: 12:05
 */

namespace App\AdminModule\Presenters;


use App\CoreModule\Model\ArticleManager;
use App\Factories\ArticleEditorFormFactory;
use App\Factories\ArticleImagesFormFactory;
use App\Model\Utils;
use App\Presenters\BasePresenter;
use Nette\Application\UI\Form;

/**
 * Class ArticlePresenter
 * @package App\AdminModule\Presenters
 */
class ArticlePresenter extends BasePresenter
{
    /** @var ArticleEditorFormFactory */
    private $articleEditorFormFactory;
    /** @var ArticleImagesFormFactory */
    private $articleImagesFormFactory;

    /**
     * ArticlePresenter constructor.
     * @param ArticleEditorFormFactory $articleEditorFormFactory
     */
    public function __construct(ArticleEditorFormFactory $articleEditorFormFactory, ArticleImagesFormFactory $articleImagesFormFactory)
    {
        parent::__construct();
        $this->articleEditorFormFactory = $articleEditorFormFactory;
        $this->articleImagesFormFactory = $articleImagesFormFactory;
    }

    public function startup(): void
    {
        parent::startup();
        $this->logInRequired();
    }

    /**
     * Removes article
     * @param string $url
     * @throws \Nette\Application\AbortException
     */
    public function actionRemove(string $url): void
    {
        $this->editorPermissionsRequired();

        $this->articleManager->removeArticle($url);
        $this->flashMessage('Článek byl úspěšně odstraněn.', 'success');
        $this->redirect(':Admin:Article:list');
    }

    /**
     * Article Editor
     * @param string $url
     * @throws \Nette\Application\AbortException
     */
    public function renderEditor(string $url = null): void
    {
        $this->editorPermissionsRequired();

        if ($url && $article = $this->articleManager->getArticleByUrl($url)) {
            $this['editorForm']->setDefaults($article);
            $this['editorForm']->setDefaults([ArticleManager::COLUMN_DATETIME => Utils::formatDate($article->datetime)]);
        }
    }

    /**
     * Displays all articles
     */
    public function renderList(): void
    {
        $this->editorPermissionsRequired();

        //Načte články z databáze a předá je šabloně
        $articles = $this->articleManager->getArticles();
        $this->template->articles = $articles;
    }

    /**
     * @return Form
     */
    protected function createComponentEditorForm(): Form
    {
        $form = $this->articleEditorFormFactory->create();
        $form->onSuccess[] = function (Form $form, array $values) {
            $values[ArticleManager::COLUMN_AUTHOR] = $this->user->getIdentity()->username;
            $this->articleManager->saveArticle($values);
            $this->flashMessage('Článek byl úspěšně uložen.', 'success');
            $this->redirect(':Core:Article:', $values['url']);
        };
        return $form;
    }

    /**
     * @return Form
     */
    protected function createComponentArticleImagesForm(): Form
    {
        return $this->articleImagesFormFactory->create();
    }

    /**
     * @param string $url
     * @param bool $visibility
     * @throws \Nette\Application\AbortException
     */
    public function handleVisibleInMenu(string $url, bool $visibility): void
    {
        $this->articleManager->setVisibilityInMenu($url, $visibility);
        $this->redirectAjax(['flashes', 'articlesTable']);
    }
}
