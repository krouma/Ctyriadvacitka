<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 5.3.17
 * Time: 12:35
 */

namespace App\AdminModule\Presenters;


use App\Factories\ArticleImagesFormFactory;
use App\Factories\NewsEditorFormFactory;
use App\Model\Utils;
use App\Presenters\BasePresenter;
use Nette\Application\UI\Form;

/**
 * Class NewsPresenter
 * @package App\AdminModule\Presenters
 */
class NewsPresenter extends BasePresenter
{
    /** @var NewsEditorFormFactory */
    private $newsEditorFormFactory;
    /** @var ArticleImagesFormFactory */
    private $articleImagesFormFactory;

    public function __construct(NewsEditorFormFactory $newsEditorFormFactory, ArticleImagesFormFactory $articleImagesFormFactory)
    {
        parent::__construct();
        $this->newsEditorFormFactory = $newsEditorFormFactory;
        $this->articleImagesFormFactory = $articleImagesFormFactory;
    }

    /**
     *
     */
    public function startup(): void
    {
        parent::startup();
        $this->logInRequired();
    }

    /**
     * Deletes news
     * @param int $id
     * @throws \Nette\Application\AbortException
     */
    public function actionRemove(int $id): void
    {
        $this->editorPermissionsRequired();

        $this->newsManager->removeNews($id);
        $this->flashMessage('Novinka byla úspěšně odstraněna.', 'success');
        $this->redirect(':Admin:News:list');
    }

    /**
     * @return Form
     */
    public function createComponentNewsEditorForm(): Form
    {
        $form = $this->newsEditorFormFactory->create();
        $form->onSuccess[] = function (Form $form, array $values): void {
            $values[$this->newsManager::COLUMN_AUTHOR] = $this->getUser()->getIdentity()->username;
            $this->newsManager->saveNews($values);
            $this->flashMessage('Novinka byla úspěšně uložena.', 'success');
            $this->redirect(':Core:News:');
        };
        return $form;
    }

    /**
     * @return Form
     */
    protected function createComponentArticleImagesForm(): Form
    {
        return $this->articleImagesFormFactory->create();
    }

    /**
     * Edits news
     * @param int $id
     */
    public function renderEditor(int $id = null): void
    {
        $this->editorPermissionsRequired();

        if ($id && $article = $this->newsManager->getNewsById($id)) {
            $this['newsEditorForm']->setDefaults($article);
            $this['newsEditorForm']->setDefaults(['datetime' => Utils::formatDate($article->datetime)]);
        }
    }

    /**
     * List news for admins
     */
    public function renderList(): void
    {
        $this->editorPermissionsRequired();

        //Načte články z databáze a předá je šabloně
        $news = $this->newsManager->getNews();
        $this->template->news = $news;
    }
}