<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: krouma
 * Date: 15.3.17
 * Time: 15:31
 */

namespace App\AdminModule\Model;


use App\Model\BaseManager;
use Nette\Database\Context;
use Nette\Utils\Finder;

/**
 * Class ImageManager
 * @property string wwwDir
 * @package AdminModule\Model
 */
class ImageManager extends BaseManager
{
    /** @var string */
    private $wwwDir;

    /**
     * ImageManager constructor.
     * @param string $wwwDir
     * @param Context $database
     */
    public function __construct(string $wwwDir, Context $database)
    {
        parent::__construct($database);
        $this->wwwDir = $wwwDir;
    }

    /**
     * @return array
     */
    public function getHeaderImageNames(): array
    {
        $dir = $this->wwwDir . '/img/top/';
        $images = [];
        foreach (Finder::findFiles('*.png', '*.jpg')->from($dir) as $name => $image) {
            $images[] = $image->getFilename();
        }
        return $images;
    }

    /**
     * @param string $path
     * @return bool
     */
    public function deleteImage(string $path): bool
    {
        return unlink($path);
    }
}