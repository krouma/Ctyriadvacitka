<?php
/** @noinspection PhpUnhandledExceptionInspection */
declare(strict_types=1);

namespace App\Presenters;

use App\AdminModule\Model\ImageManager;
use App\CoreModule\Model\ArticleManager;
use App\CoreModule\Model\AuthenticatorManager;
use App\CoreModule\Model\AuthorizatorManager;
use App\CoreModule\Model\NewsManager;
use App\CoreModule\Model\UserManager;
use App\Model\ProjectManager;
use Nette\Application\UI\Presenter;
use Nette\Database\Context;
use Nette\Security\User;


/**
 * Base presenter for all application presenters.
 * @package App\Presenters
 */
abstract class BasePresenter extends Presenter
{

    /** @var  User instance of User */
    protected $user;
    /** @var UserManager instance of UserManager @inject */
    public $userManager;
    /** @var  ArticleManager instance of ArticleManager @inject */
    public $articleManager;
    /** @var  NewsManager instance of NewsManager @inject */
    public $newsManager;
    /** @var  ImageManager @inject */
    public $imageManager;
    /** @var  ProjectManager @inject */
    public $projectManager;
    /** @var Context Instance of class to work with DB @inject*/
    public $database;
    /** @var AuthenticatorManager @inject */
    public $authenticator;
    /** @var AuthorizatorManager @inject */
    public $authorizator;

    public function startup(): void
    {
        parent::startup();
        $this->user = $this->getUser();
        $this->user->setAuthenticator($this->authenticator);
        $this->user->setAuthorizator($this->authorizator);
    }

    public function beforeRender(): void
    {
        parent::beforeRender();
        if ($this->template->isLoggedIn = $this->user->isLoggedIn()) {
            $this->template->loggedUser = $this->user->getIdentity();
        }
        $this->template->isUserAdmin = $this->user->isInRole('admin');
        $this->template->isUserEditor = $this->user->isInRole('editor') || $this->user->isInRole('admin');
        if ($this->presenter !== null) {
            $this->template->presenter = $this->presenter;
        }

        $this->template->webName = $this->projectManager->getParameter('webName');
        $this->template->webDescription = $this->projectManager->getParameter('webDescription');

        //header picture
        $images = $this->imageManager->getHeaderImageNames();
        $imagesCount = count($images);
        if ($imagesCount === 0) {
            $this->template->headerPicture = null;
        } else {
            $topRandom = random_int(0, $imagesCount - 1);
            $this->template->headerPicture = $images[$topRandom];
        }

        //articles/news to show in navbar menu
        if (!isset($this->template->articlesInMenu)) {
            $this->template->articlesInMenu = $this->projectManager->getArticlesInMenu();
        }
    }

    public function logInRequired(): void
    {
        if (!$this->user->isLoggedIn()) {
            $this->flashMessage('Nejste přihlášen!', 'warning');
            $this->redirect(':Core:Session:signIn');
        }
    }

    public function editorPermissionsRequired(): void
    {
        if (!$this->user->isInRole('editor') && !$this->user->isInRole('admin')) {
            $this->flashMessage('Na tuto akci musíte být redaktor.', 'warning');
            $this->redirect(':Core:User:');
        }
    }

    public function adminPermissionsRequired(): void
    {
        if (!$this->user->isInRole('admin')) {
            $this->flashMessage('Na tuto akci musíte být administrátor.', 'warning');
            $this->redirect(':Core:User:');
        }
    }

    /**
     * @param array|null $snippets
     * @param null|string $destination
     * @throws \Nette\Application\AbortException
     */
    public function redirectAjax(array $snippets = null, string $destination = null): void
    {
        if ($this->isAjax()) {
            if ($snippets === null) {
                $this->redrawControl();
            } else {
                foreach ($snippets as $snippet) {
                    $this->redrawControl($snippet);
                }
            }
        } else {
            if ($destination === null) {
                $this->redirect('this');
            } else {
                $this->redirect('this', $destination);
            }
        }
    }
}
